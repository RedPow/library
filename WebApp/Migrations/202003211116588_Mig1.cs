namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mig1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookss",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Author = c.String(),
                        DateOfEditions = c.String(),
                        DateTimeOfBegin = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StudentBooks",
                c => new
                    {
                        Student_Id = c.Int(nullable: false),
                        Book_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Student_Id, t.Book_Id })
                .ForeignKey("dbo.Students", t => t.Student_Id, cascadeDelete: true)
                .ForeignKey("dbo.Bookss", t => t.Book_Id, cascadeDelete: true)
                .Index(t => t.Student_Id)
                .Index(t => t.Book_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentBooks", "Book_Id", "dbo.Bookss");
            DropForeignKey("dbo.StudentBooks", "Student_Id", "dbo.Students");
            DropIndex("dbo.StudentBooks", new[] { "Book_Id" });
            DropIndex("dbo.StudentBooks", new[] { "Student_Id" });
            DropTable("dbo.StudentBooks");
            DropTable("dbo.Students");
            DropTable("dbo.Books");
        }
    }
}
