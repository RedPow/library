namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _14 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Books", name: "User_Id", newName: "User_Id_Id");
            RenameIndex(table: "dbo.Books", name: "IX_User_Id", newName: "IX_User_Id_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Books", name: "IX_User_Id_Id", newName: "IX_User_Id");
            RenameColumn(table: "dbo.Books", name: "User_Id_Id", newName: "User_Id");
        }
    }
}
