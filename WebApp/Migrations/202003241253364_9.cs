namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _9 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Books_Id", "dbo.Books");
            DropIndex("dbo.AspNetUsers", new[] { "Books_Id" });
            DropColumn("dbo.AspNetUsers", "Books_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Books_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Books_Id");
            AddForeignKey("dbo.AspNetUsers", "Books_Id", "dbo.Books", "Id");
        }
    }
}
