namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentBooks", "Student_Id", "dbo.Students");
            DropForeignKey("dbo.StudentBooks", "Book_Id", "dbo.Books");
            DropIndex("dbo.StudentBooks", new[] { "Student_Id" });
            DropIndex("dbo.StudentBooks", new[] { "Book_Id" });
            AddColumn("dbo.Books", "Student_Id", c => c.Int());
            CreateIndex("dbo.Books", "Student_Id");
            AddForeignKey("dbo.Books", "Student_Id", "dbo.Students", "Id");
            DropTable("dbo.StudentBooks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.StudentBooks",
                c => new
                    {
                        Student_Id = c.Int(nullable: false),
                        Book_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Student_Id, t.Book_Id });
            
            DropForeignKey("dbo.Books", "Student_Id", "dbo.Students");
            DropIndex("dbo.Books", new[] { "Student_Id" });
            DropColumn("dbo.Books", "Student_Id");
            CreateIndex("dbo.StudentBooks", "Book_Id");
            CreateIndex("dbo.StudentBooks", "Student_Id");
            AddForeignKey("dbo.StudentBooks", "Book_Id", "dbo.Books", "Id", cascadeDelete: true);
            AddForeignKey("dbo.StudentBooks", "Student_Id", "dbo.Students", "Id", cascadeDelete: true);
        }
    }
}
