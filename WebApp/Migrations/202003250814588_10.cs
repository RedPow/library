namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Books", "Student_Id", c => c.Int());
            CreateIndex("dbo.Books", "Student_Id");
            AddForeignKey("dbo.Books", "Student_Id", "dbo.Students", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "Student_Id", "dbo.Students");
            DropIndex("dbo.Books", new[] { "Student_Id" });
            DropColumn("dbo.Books", "Student_Id");
            DropTable("dbo.Students");
        }
    }
}
