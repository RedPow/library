namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "URl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "URl");
        }
    }
}
