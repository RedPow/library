namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _8 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Books", "Student_Id", "dbo.Students");
            DropIndex("dbo.Books", new[] { "Student_Id" });
            AddColumn("dbo.AspNetUsers", "Count", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Books_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Books_Id");
            AddForeignKey("dbo.AspNetUsers", "Books_Id", "dbo.Books", "Id");
            DropColumn("dbo.Books", "Student_Id");
            DropTable("dbo.Students");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Books", "Student_Id", c => c.Int());
            DropForeignKey("dbo.AspNetUsers", "Books_Id", "dbo.Books");
            DropIndex("dbo.AspNetUsers", new[] { "Books_Id" });
            DropColumn("dbo.AspNetUsers", "Books_Id");
            DropColumn("dbo.AspNetUsers", "Count");
            CreateIndex("dbo.Books", "Student_Id");
            AddForeignKey("dbo.Books", "Student_Id", "dbo.Students", "Id");
        }
    }
}
