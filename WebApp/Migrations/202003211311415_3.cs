namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "User", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "User");
        }
    }
}
