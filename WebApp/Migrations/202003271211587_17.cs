namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _17 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Books", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Books", new[] { "User_Id" });
            AddColumn("dbo.Books", "User", c => c.String());
            DropColumn("dbo.Books", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "User_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.Books", "User");
            CreateIndex("dbo.Books", "User_Id");
            AddForeignKey("dbo.Books", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
