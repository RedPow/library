﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class BookController : Controller
    {
        private ApplicationDbContext _contextBooks;


        public BookController()
        {
            _contextBooks = new ApplicationDbContext();
        }

        // GET: Books
        public ActionResult Index()
        {
            var books =_contextBooks.Books.ToList();
            if (books != null)
            {
                return View(books);
            }
            return RedirectToAction("Create");
        }

        public ActionResult TakeBackBook(int id)
        {
            var user = _contextBooks.Books.FirstOrDefault(book => book.Id == id).User;

            if (user != null)
            {
                string NameUser = user;
                _contextBooks.Users.First(o => o.UserName == NameUser).Count -= 1;
                _contextBooks.Books.FirstOrDefault(book => book.Id == id).User = null;
                _contextBooks.SaveChanges();
                return RedirectToAction("index", "Book");
            }
            return RedirectToAction("index", new { Message = "Ошибка: пользователя нет" });
        }

        public ActionResult Confirmation(int id)
        {
            return PartialView(id);
        }

        public ActionResult UserIndicate(int id)
        {
            var book = _contextBooks.Books.FirstOrDefault(o => o.Id == id);
            if(book.User!=null)
            {
                return PartialView(book);
            }
            return PartialView(book);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UserIndicate(Book model)
        {
            if (_contextBooks.Users.FirstOrDefault(o=>o.UserName == model.User)!= null)
            {
                var books = _contextBooks.Books.ToList();
                var book = _contextBooks.Books.FirstOrDefault(o => o.Id == model.Id);
                if (book.User == null)
                {
                    model.User = _contextBooks.Users.FirstOrDefault(o => o.UserName == model.User).Email;
                    book.User = model.User;

                    var uers = _contextBooks.Users.FirstOrDefault(o => o.UserName == model.User).Count += 1;

                    book.PastUsers += 1;
                    book.DateTimeOfBegin = DateTime.Now.ToShortDateString();

                    _contextBooks.SaveChanges();
                    return RedirectToAction("index", "Book");
                }
            }
            return RedirectToAction("index", "Book");
        }


        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            var book = _contextBooks.Books.FirstOrDefault(o => o.Id == id);
            return PartialView(book);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Book/Create
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Book model)
        {
            if (ModelState.IsValid)
            {
                var book = new Book { Name = model.Name, Author = model.Author, URl = model.URl, DateOfEditions = model.DateOfEditions };
                _contextBooks.Books.Add(book);
                _contextBooks.SaveChanges();
            }
            return RedirectToAction("Index", "Book");
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {
            var book = _contextBooks.Books.FirstOrDefault(o => o.Id == id);
            if(book.User != null)
            {
                return PartialView(book);
            }
            return PartialView(book);
        }

        [HttpPost]
        public ActionResult Edit(Book book)
        {
            try
            {
                _contextBooks.Entry(book).State = System.Data.Entity.EntityState.Modified;
                _contextBooks.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            if (User.Identity.Name == "11051969dahul@mail.ru")
            {
                var Book = _contextBooks.Books.FirstOrDefault(o => o.Id == id);
                if (Book.User == null) {
                    _contextBooks.Books.Remove(Book);
                    _contextBooks.SaveChanges();
                }
            }
            return RedirectToAction("Index", "Book");
        }
    }
}