﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class StudentController : Controller
    {
        private ApplicationDbContext _context;

        public StudentController()
        {
            _context = new ApplicationDbContext();
            _context.Users.ToList();
        }
        // GET: Students
        public ActionResult Index()
        {
            var users = _context.Users.ToList();
            return View(users);
        }

        public ActionResult Info(string id)
        {

            var user = _context.Users.FirstOrDefault(o => o.Id == id);
            var books = _context.Books.ToList().Where(o => o.User == user.UserName).ToList();
            if (books != null)
            {
                return View(books);
            }
            
            return RedirectToAction("Idex","Book");
        }

        public ActionResult DeleteStudent(string id)
        {
            var student = _context.Users.FirstOrDefault(o => o.Id == id);
            ApplicationUser message = null;
            if (student != null && student.Count==0)
            {
                message = _context.Users.Remove(student);
                _context.SaveChanges();
            }
            return View(message);
        }
    }
}