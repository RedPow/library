﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class Book
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Автор")]
        public string Author { get; set; }

        [Required]
        [Display(Name = "Дата публикации")]
        [DataType(DataType.Date)]
        public string DateOfEditions { get; set; }

        [Display(Name = "Кол-во прошлых пользователь")]
        public int PastUsers { get; set; }

        [Display(Name = "Дата начала пользования")]
        public string DateTimeOfBegin { get; set; }

        [Display(Name = "Настоящий пользователь")]
        public string User { get; set; }
        
        [Required]
        [Display(Name = "Обложка книги")]
        [Url]
        public string URl { get; set; }
    }
}